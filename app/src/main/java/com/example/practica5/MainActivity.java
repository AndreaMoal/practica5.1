package com.example.practica5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtcedula, txtapellidos, txtnombres, txtdatos;
  //  TextView txtdatos;
    Button btnescribir, btnleer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtcedula = (EditText) findViewById(R.id.txtcedula);
        txtapellidos = (EditText) findViewById(R.id.txtapellidos);
        txtnombres = (EditText) findViewById(R.id.txtnombres);
       txtdatos = (EditText) findViewById(R.id.txtdatos);
       // txtdatos = (EditText) findViewById(R.id.txtdatos);
        btnescribir = (Button) findViewById(R.id.btnescribir);
        btnleer = (Button) findViewById(R.id.btnleer);

        btnleer.setOnClickListener(this);
        btnescribir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnescribir:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(txtcedula.getText().toString()+ ","+ txtapellidos.getText().toString()+","+txtnombres.getText().toString());
                    escritor.flush();
                    escritor.close();
                }catch (Exception ex){
                    ex.printStackTrace();
                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                Toast.makeText(view.getContext(), "Los datos se grabaron con éxito", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnleer:
                try{
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String[] listaPersonas = datos.split(",");
                    txtdatos.setText("");
                    for (int i=0; i< listaPersonas.length; i++){
                        txtdatos.append(listaPersonas[i] + "\n ");
                        //cajaDatos.append(listaPersonas[i].split(",")[0]+ "" +listaPersonas[i].split(",")[1]+""+listaPersonas[i].split(",")[2]);
                    }
                    lector.close();
                }catch (Exception ex){
                    Log.e("archivo MI","Error en la lectura del archivo");
                }
                break;

        }

    }
}